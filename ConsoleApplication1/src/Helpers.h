#pragma once

#include <iostream>
#include <string>
#include <string_view>

namespace learn_ue
{
	template<class T>
	inline static void func_print_line(const T& _input) { std::cout << _input << '\n'; }

	inline static float func_sq_sum(float _v_0, float _v_1) { return (_v_0 += _v_1, _v_0 * _v_0); }
}